<?php
/**
 * Contains the functions that will register the block into WordPress.
 *
 * @since 1.0
 *
 * @package scorpiotek-social-media-blocks
 */

/**
 * Register_scorpiotek_category_block Creates the 'scorpiotek-blocks' category.
 * Later on when the actual block is created, we specify the 'scorpiotek-blocks'
 * category created below for the block to live under.
 *
 * @param  array $categories the current block categories.
 * @param  array $post
 *
 * @return void
 */
function register_scorpiotek_category_block( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'scorpiotek-blocks',
				'title' => __( 'ScorpioTek Blocks', 'scorpiotek' ),
				'icon'  => 'dashicons-welcome-learn-more',
			),
		)
	);
}
add_filter( 'block_categories', 'register_scorpiotek_category_block', 10, 2 );

/**
 * Register_social_media_buttons registers the actual social media button block with WordPress.
 *
 * @return void
 */
function register_social_media_buttons() {
	acf_register_block(
		array(
			'name'            => 'scorpio_tek_social_media_buttons',
			'title'           => __( 'Button', 'scorpiotek' ),
			'description'     => __( 'A blue button with yellow text', 'scorpiotek' ),
			'render_template' => plugin_dir_path( __FILE__ ) . 'partials/social-media-buttons.php',
			'category'        => 'scorpiotek-blocks',
			'keywords'        => [ 'button' ],
			'mode'            => 'edit',
			'icon'            => array(
				'foreground' => '#004A99',
				'background' => '#FFEE00',
				'src'        => 'images-alt2',
			),
		)
	);
}
add_action( 'acf/init', 'register_social_media_buttons' );