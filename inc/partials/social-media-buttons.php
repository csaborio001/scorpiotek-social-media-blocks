<?php
/**
 * Template that defines how the blocks are going to be displayed. We fetch the values from
 * the ACF fields the user defined in runtime and display their content here.
 *
 * @since 1.0
 *
 * @package scorpiotek-social-media-blocks
 */

$button_text     = esc_html( get_field( 'button_text' ) );
$button_link_obj = get_field( 'button_link' );
$button_link     = esc_url( $button_link_obj['url'] );
$link_target     = '_self';

/** We only change the target attribute if what was fetched for the link is an array object
 * and if the index of 'target' actually exists we set the target as such. Otherwise we just
 * leave the target to _self.
 */
if ( is_array( $button_link_obj ) && array_key_exists( 'target', $button_link_obj ) ) {
	$link_target = $button_link_obj['target'];
}
?>

<a href="<?php echo esc_url( $button_link ); ?>" target="<?php echo esc_html( $link_target ); ?>">
	<div class="strong-button" style="display:inline-block">
		<?php echo $button_text; ?>
	</div>
</a>
