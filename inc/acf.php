<?php
/**
 * Contains the definition of all the ACF fields used by the Social Justice Network.
 *
 * @since 1.0
 *
 * @package scorpiotek-social-media-blocks
 */

use \StoutLogic\AcfBuilder\FieldsBuilder;
use SocialJustice\ActionNetworkConnector;

if ( class_exists( FieldsBuilder::class ) ) {
	add_action( 'after_setup_theme', 'create_social_media_button_fields' );
}

/**
 * Creates the fields that will be used in the template to render choices that the
 * user will have, such as specifying the link of the button and the label that the
 * button should have.
 *
 * @return void
 */
function create_social_media_button_fields() {
	$social_media_button_fields = new FieldsBuilder( 'social_media_button' );
	$social_media_button_fields

	->addText(
		'button_text',
		array(
			'label'         => 'Button Text',
			'instructions'  => 'The text to be displayed on the button.',
			'default_value' => 'Button Text',
			'required'      => 1,
			'wrapper'       => array(
				'width' => 50,
			),
		)
	)

	->addLink(
		'button_link',
		array(
			'label'         => 'Button Link',
			'instructions'  => 'The link of the button',
			'return_format' => 'array',
			'required'      => 1,
			'wrapper'       => array(
				'width' => 50,
			),
		)
	)

	/** This has to match the type that is defined inside acf-blocks. For some obscure reason
	 * this has to be defined using dashes even though the block name is defined using underscores.
	 * I'll buy you a beer if you can find the reasoning behind this.
	 */
	->setLocation( 'block', '==', 'acf/scorpio-tek-social-media-buttons' );

	add_action(
		'acf/init',
		function() use ( $social_media_button_fields ) {
			acf_add_local_field_group( $social_media_button_fields->build() );
		}
	);
}
