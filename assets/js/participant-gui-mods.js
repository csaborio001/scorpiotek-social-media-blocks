/**
 * Contains the functions that modify various GUI elements in the 
 * WPAdmin interface.
 * @version 1.0
 */
jQuery(document).ready(function($){
    var removeAdminControls = form_data.remove_admin_controls;
    setPostTitleReadOnly($);
    disableChangeStateButtonIfMatched($);
    if ( removeAdminControls ) {
        hideRepeaterStateButtons($);
        hideEditRemoveLinkButtons($);
    }
    changeLinkTargetToSelf($);
    makeNoteTextAreaReadOnly($);
    disableAddNoteButtonWhenClicked($);
});

/**
 * The title property of all entities need to be set to 
 * read only. This code snippet sets the text to read
 * only.
 *
 * @param {*} $ the jQuery class
 */
function setPostTitleReadOnly($) {
    $('#title').prop('readonly', true);
}

/**
 * When the participant state is under a match, the 
 * change state button needs to be disabled as every change
 * is done by forms.
 *
 * @param {*} $ the jQuery class
 */
function disableChangeStateButtonIfMatched($) {
    var match_states = ['friendship_current', 'friendship_requested', 'match_pending', 'match_in_progress' ];
    var needle = form_data.state;
    if ( -1 !== $.inArray( needle, match_states ) ) {
        $('#change_volunteer__state_button').prop('disabled',true)
    }
}

function hideRepeaterStateButtons($) {
    // Gets rid of Add State History Button.
    $('a:contains("Add State History")').hide()
    // Gets rid of all columns that have + / - signs.
    $('.acf-repeater').find('.acf-row-handle').remove();
}

function hideEditRemoveLinkButtons($) {
    $('.link-wrap > .-pencil').remove();
    $('.link-wrap > .-cancel').remove();
}

function changeLinkTargetToSelf($) {
    $('.link-url').attr('target','_self')
}

function makeNoteTextAreaReadOnly($) {
    $('.acf-field-volunteer-fields-entity-notes-control').find('tr[data-id^="row"]').find('textarea').prop('readonly', true);
}

function disableAddNoteButtonWhenClicked($) {
    var addNoteButton =  $('a:contains("Add Note")');
    addNoteButton.click(function() {
        addNoteButton.hide();
    });
}