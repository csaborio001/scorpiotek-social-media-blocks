<?php
/**
 * Plugin Name: ScorpioTek Social Media Blocks
 * Description: Provides Social Media sharing blocks that can link to other pages.
 *
 * @since  1.0
 *
 * @package scorpiotek-social-media-blocks
 * Version: 1.0
 * Text Domain: scorpiotek
 **/

namespace ScorpioTek\Plugins;

// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once 'vendor/autoload.php';

/**
 * Creates the custom post types required for the plugin.
 */
// require_once 'inc/content-types.php';
/**
 * Renders all the ACF fields used bu the plugin.
 */
require_once 'inc/acf.php';
/**
 * Contains the code that defines the ACF Blocks.
 */
require_once 'inc/acf-blocks.php';

/**
 * Loads the scripts and stylesheets used by the plugin
 */
// require_once 'inc/plugin-assets.php';


